﻿using BepInEx;
using BepInEx.Configuration;
using MonoMod.Cil;
using RoR2;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using UnityEngine;
using UnityEngine.Networking;

namespace AutoItemPickup
{
    [BepInDependency("com.bepis.r2api")]
    [BepInPlugin("com.kuberoot.autoitempickup", "AutoItemPickup", "1.2.2")]
    
    public class AutoItemPickup : BaseUnityPlugin
    {

        public enum Mode
        {
            Sequential,
            Random,
            Closest,
            LeastItems
        }

        public enum Cause
        {
            Teleport,
            Drop
        }

        public void Awake()
        {
            ModConfig.InitConfig(Config, Logger);

            On.RoR2.SceneExitController.Begin += OnTeleportBegin;
            On.RoR2.GenericPickupController.Start += OnItemSpawn;

            On.RoR2.PlayerCharacterMasterController.OnBodyDeath += (orig, self) => { orig(self); UpdateTargets(); };
            On.RoR2.PlayerCharacterMasterController.OnBodyStart += (orig, self) => { orig(self); UpdateTargets(); };

            On.RoR2.ShopTerminalBehavior.DropPickup += OnDropPickup;
            IL.RoR2.PickupDropletController.CreatePickupDroplet += ModifyCreatePickupDroplet;
            IL.RoR2.PickupDropletController.OnCollisionEnter += ModifyDropletCollision;

            On.RoR2.PlayerCharacterMasterController.Awake += OnPlayerAwake;

            PlayerCharacterMasterController.onPlayerAdded += (player) => UpdateTargets();
            PlayerCharacterMasterController.onPlayerRemoved += (player) => UpdateTargets();

            ModConfig.distributeToDeadPlayers.OnValueChanged += (value) => UpdateTargets();

            ModConfig.distributionModeDrop.OnValueChanged += (value) => dropDistributor = ItemDistributor.GetItemDistributor(value);
        }

        private void OnPlayerAwake(On.RoR2.PlayerCharacterMasterController.orig_Awake orig, PlayerCharacterMasterController self)
        {
            orig(self);

            if(NetworkServer.active)
            {
                var master = self.master;
                if(master)
                {
                    master.onBodyStart += obj => UpdateTargets();
                }
            }
        }

        private void ModifyDropletCollision(ILContext il)
        {
            ILCursor cursor = new ILCursor(il);

            cursor.GotoNext(MoveType.After, i => i.MatchCall<UnityEngine.Object>("Instantiate"));
            cursor.Emit(Mono.Cecil.Cil.OpCodes.Dup);
            cursor.Emit(Mono.Cecil.Cil.OpCodes.Ldarg_0);
            cursor.EmitDelegate<Action<UnityEngine.GameObject, PickupDropletController>>((obj, self) =>
            {
                OverrideDistributorBehaviour behaviour = self.GetComponent<OverrideDistributorBehaviour>();
                if(behaviour)
                {
                    OverrideDistributorBehaviour newBehaviour = obj.AddComponent<OverrideDistributorBehaviour>();
                    newBehaviour.distributor = behaviour.distributor;
                }
            });
        }

        private void ModifyCreatePickupDroplet(MonoMod.Cil.ILContext il)
        {
            ILCursor cursor = new ILCursor(il);

            cursor.GotoNext(MoveType.After, i => i.MatchCall<UnityEngine.Object>("Instantiate"));
            cursor.Emit(Mono.Cecil.Cil.OpCodes.Dup);
            cursor.EmitDelegate<Action<UnityEngine.GameObject>>(obj =>
            {
                if (dropletDistributorOverride != null)
                {
                    OverrideDistributorBehaviour behaviour = obj.AddComponent<OverrideDistributorBehaviour>();
                    behaviour.distributor = dropletDistributorOverride;
                }
            });
        }

        public static ItemDistributor dropletDistributorOverride;
        private void OnDropPickup(On.RoR2.ShopTerminalBehavior.orig_DropPickup orig, ShopTerminalBehavior self)
        {
            if (ModConfig.printerOverrideTarget)
            {
                PurchaseInteraction interaction = self.GetComponent<PurchaseInteraction>();
                if(interaction && interaction.costType == CostTypeIndex.WhiteItem ||
                    interaction.costType == CostTypeIndex.GreenItem ||
                    interaction.costType == CostTypeIndex.RedItem)
                {
                    PlayerCharacterMasterController target = interaction.lastActivator?.GetComponent<CharacterBody>()?.master?.GetComponent<PlayerCharacterMasterController>();
                    
                    if(target)
                        dropletDistributorOverride = new ItemDistributors.FixedTargetDistributor(target);
                }
            }

            orig(self);
            dropletDistributorOverride = null;
        }

        private static MethodInfo GenericPickupController_GrantItem = typeof(GenericPickupController).GetMethod("GrantItem", BindingFlags.Instance | BindingFlags.NonPublic);

        internal static void GrantItem(GenericPickupController item, Inventory inventory)
        {
            GenericPickupController_GrantItem.Invoke(item, new object[] { null, inventory });
        }

        private static bool ShouldDistribute(GenericPickupController pickup, Cause cause)
        {
            if (pickup == null || pickup.pickupIndex == null || pickup.pickupIndex.itemIndex == ItemIndex.None)
                return false;

            PickupIndex index = pickup.pickupIndex;
            ItemDef itemDef = ItemCatalog.GetItemDef(index.itemIndex);

            if (itemDef == null)
                return false;

            if (cause == Cause.Drop && ModConfig.dropItemBlacklist.HasItem(itemDef.itemIndex))
                return false;

            if (cause == Cause.Teleport && ModConfig.teleportItemBlacklist.HasItem(itemDef.itemIndex))
                return false;

            switch (itemDef.tier)
            {
                case ItemTier.Tier1:
                    return (cause == Cause.Drop) ? ModConfig.distributeWhiteItemsDrop : ModConfig.distributeWhiteItemsTeleport;
                case ItemTier.Tier2:
                    return (cause == Cause.Drop) ? ModConfig.distributeGreenItemsDrop : ModConfig.distributeGreenItemsTeleport;
                case ItemTier.Tier3:
                    return (cause == Cause.Drop) ? ModConfig.distributeRedItemsDrop : ModConfig.distributeRedItemsTeleport;
                case ItemTier.Lunar:
                    return (cause == Cause.Drop) ? ModConfig.distributeLunarItemsDrop : ModConfig.distributeLunarItemsTeleport;
                case ItemTier.Boss:
                    return (cause == Cause.Drop) ? ModConfig.distributeBossItemsDrop : ModConfig.distributeBossItemsTeleport;
            }

            return false;
        }

        private bool dropDistributorNeedsUpdate;
        private ItemDistributor dropDistributor;
        private ItemDistributor teleportDistributor;

        private void UpdateTargets()
        {
            dropDistributorNeedsUpdate = true;
        }

        private void PreDistributeItem(Cause cause)
        {
            if (cause == Cause.Teleport)
                teleportDistributor = ItemDistributor.GetItemDistributor(ModConfig.distributionModeTeleport);
            
            if (cause == Cause.Drop && dropDistributor == null)
                dropDistributor = ItemDistributor.GetItemDistributor(ModConfig.distributionModeDrop);

            if(cause == Cause.Drop && dropDistributorNeedsUpdate)
            {
                dropDistributorNeedsUpdate = false;
                dropDistributor?.UpdateTargets();
            }
        }

        private void DistributeItem(GenericPickupController item, Cause cause)
        {
            ItemDistributor distributor;
            switch(cause)
            {
                default: // default shouldn't happen, but otherwise distributor is potentially unassigned
                case Cause.Teleport:
                    distributor = teleportDistributor;
                    break;
                case Cause.Drop:
                    distributor = dropDistributor;
                    break;
            }

            OverrideDistributorBehaviour overrideDistributorBehaviour = item.GetComponent<OverrideDistributorBehaviour>();
            if (overrideDistributorBehaviour != null && overrideDistributorBehaviour.distributor != null && overrideDistributorBehaviour.distributor.IsValid())
                distributor = overrideDistributorBehaviour.distributor;

            try
            {
                distributor.DistributeItem(item);
            }
            catch(Exception e)
            {
                Debug.Log($"Caught AutoItemPickup distributor exception:\n{e}\n{e.StackTrace}");
            }
        }

        private void OnItemSpawn(On.RoR2.GenericPickupController.orig_Start orig, GenericPickupController self)
        {
            if (UnityEngine.Networking.NetworkServer.active && ModConfig.distributeOnDrop)
            {
                if (ShouldDistribute(self, Cause.Drop))
                {
                    PreDistributeItem(Cause.Drop);
                    DistributeItem(self, Cause.Drop);
                }
            }
            orig(self);
        }

        private void OnTeleportBegin(On.RoR2.SceneExitController.orig_Begin orig, SceneExitController self)
        {
            if (UnityEngine.Networking.NetworkServer.active && ModConfig.distributeOnTeleport)
            {
                PreDistributeItem(Cause.Teleport);

                var originalPickups = InstanceTracker.GetInstancesList<GenericPickupController>();

                GenericPickupController[] pickups = new GenericPickupController[originalPickups.Count];

                originalPickups.CopyTo(pickups);
                
                foreach(var pickup in pickups)
                {
                    if(ShouldDistribute(pickup, Cause.Teleport))
                    {
                        teleportDistributor.DistributeItem(pickup);
                    }
                }
            }
            orig(self);
        }
    }
}