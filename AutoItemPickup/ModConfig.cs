﻿using BepInEx.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using AutoItemPickup.Extensions;
using RoR2;
using System.Linq;

namespace AutoItemPickup
{
    public static class ModConfig
    {
        private static BepInEx.Configuration.ConfigFile config;
        private static BepInEx.Logging.ManualLogSource logger;

        public class ItemSetConfigWrapper
        {
            private ConfigWrapper<string> wrapper;
            private HashSet<ItemWrapper> items = new HashSet<ItemWrapper>();

            private static Dictionary<string, ItemWrapper> itemsByName = new Dictionary<string, ItemWrapper>();
            private static Dictionary<ItemIndex, ItemWrapper> itemsByIndex = new Dictionary<ItemIndex, ItemWrapper>();

            private struct ItemWrapper
            {
                public ItemIndex itemIndex;
                public string EN_Name;

                public bool useNameInsteadOfIndex;

                public ItemWrapper(ItemIndex itemIndex, string EN_Name) : this()
                {
                    this.itemIndex = itemIndex;
                    this.EN_Name = EN_Name;
                    useNameInsteadOfIndex = false;
                }

                public override bool Equals(object obj)
                {
                    if (!(obj is ItemWrapper))
                    {
                        return false;
                    }

                    var wrapper = (ItemWrapper)obj;
                    return itemIndex == wrapper.itemIndex &&
                           EN_Name == wrapper.EN_Name;
                }

                public override int GetHashCode()
                {
                    var hashCode = 2130968230;
                    hashCode = hashCode * -1521134295 + itemIndex.GetHashCode();
                    hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(EN_Name);
                    return hashCode;
                }
            }

            static ItemSetConfigWrapper()
            {
                Language.LoadAllFilesForLanguage("en");

                foreach(var def in ItemCatalog.allItems.Select(ItemCatalog.GetItemDef))
                {
                    ItemIndex index = def.itemIndex;
                    string name = Language.GetString(def.nameToken, "en");

                    ItemWrapper wrapper = new ItemWrapper(index, name);

                    itemsByIndex[index] = wrapper;
                    
                    wrapper.useNameInsteadOfIndex = true;
                    itemsByName[name] = wrapper;
                }
            }
            
            private bool ignoreUpdate = false;

            public ItemSetConfigWrapper(ConfigWrapper<string> wrapper)
            {
                this.wrapper = wrapper;

                ParseWrapper();

                wrapper.SettingChanged += (_, __) => { if (!ignoreUpdate) ParseWrapper(); };
            }

            private void ParseWrapper()
            {
                items.Clear();
                foreach(var itemString in wrapper.Value.Split(',').Select(s => s.Trim()))
                {
                    if (itemString == "")
                        continue;

                    try
                    {
                        ItemWrapper item;
                        ItemIndex index;
                        if (Enum.TryParse(itemString, out index))
                        {
                            item = itemsByIndex[index];
                        }
                        else
                        {
                            item = itemsByName[itemString];
                        }

                        items.Add(item);
                    }
                    catch(Exception)
                    {
                        logger.LogWarning($"Invalid item in blacklist: {itemString}; ignoring");
                    }
                }
            }

            private void UpdateWrapper()
            {
                StringBuilder sb = new StringBuilder();

                foreach(var item in items)
                {
                    sb.Append(item.useNameInsteadOfIndex ? item.EN_Name : item.itemIndex.ToString());
                    sb.Append(", ");
                }

                sb.Length -= 2;

                ignoreUpdate = true;
                wrapper.Value = sb.ToString();
                ignoreUpdate = false;
            }

            public void SetHasItem(ItemIndex index, bool value)
            {
                ItemWrapper item = itemsByIndex[index];
                if (value)
                    items.Add(item);
                else
                    items.Remove(item);
            }

            public void SetHasItem(string name, bool value)
            {
                ItemWrapper item = itemsByName[name];
                if (value)
                    items.Add(item);
                else
                    items.Remove(item);
            }

            public bool HasItem(ItemIndex index)
            {
                return items.Contains(itemsByIndex[index]);
            }

            public bool HasItem(string name)
            {
                return items.Contains(itemsByName[name]);
            }
        }

        internal static void InitConfig(ConfigFile _config, BepInEx.Logging.ManualLogSource _logger)
        {
            config = _config;
            logger = _logger;

            distributeToDeadPlayers = config.Wrap("General", "DistributeToDeadPlayers", "Should items be distributed to dead players?", true);
            printerOverrideTarget = config.Wrap("General", "OverridePrinterTarget", "Should items from printers and cauldrons be distributed only to activator as long as they're a valid target?", true);

            distributeOnDrop     = config.Wrap("General", "DistributeOnDrop", "Should items be distributed when they drop?", false);
            distributeOnTeleport = config.Wrap("General", "DistributeOnTeleport", "Should items be distributed when the teleporter is activated?", true);

            distributionModeDrop = config.Wrap("OnDrop", "ItemDistributionMode", @"Decide how to distribute items among the players [Sequential, Random, Closest, LeastItems]
Sequential - Goes over all players, giving each of them one item
Random - Chooses which player receives the item randomly
Closest - Gives the item to the nearest player
LeastItems - Gives the item to the player with least total items of the item's tier", "Sequential").ToCachedConfigEntry(a =>
            {
                AutoItemPickup.Mode mode;
                if (!Enum.TryParse(a, true, out mode))
                    mode = AutoItemPickup.Mode.Sequential;
                return mode;
            }, a => a.ToString());

            distributeWhiteItemsDrop = config.Wrap("OnDrop", "DistributeWhiteItems", "Should white items be distributed?", true);
            distributeGreenItemsDrop = config.Wrap("OnDrop", "DistributeGreenItems", "Should green items be distributed?", true);
            distributeRedItemsDrop   = config.Wrap("OnDrop", "DistributeRedItems",   "Should red items be distributed?",   false);
            distributeLunarItemsDrop = config.Wrap("OnDrop", "DistributeLunarItems", "Should lunar items be distributed?", false);
            distributeBossItemsDrop  = config.Wrap("OnDrop", "DistributeBossItems",  "Should boss items be distributed?",  false);

            dropItemBlacklist = new ItemSetConfigWrapper(config.Wrap("OnDrop", "ItemBlacklist", "Comma-separated list of items to ignore; works with both english names and ItemIndex names.", ""));

            distributionModeTeleport = config.Wrap("OnTeleport", "ItemDistributionMode", @"Decide how to distribute items among the players [Sequential, Random, Closest, LeastItems]
Sequential - Goes over all players, giving each of them one item
Random - Chooses which player receives the item randomly
Closest - Gives the item to the nearest player
LeastItems - Gives the item to the player with least total items of the item's tier", "Sequential").ToCachedConfigEntry(a =>
            {
                AutoItemPickup.Mode mode;
                if (!Enum.TryParse(a, true, out mode))
                    mode = AutoItemPickup.Mode.Sequential;
                return mode;
            }, a => a.ToString());

            distributeWhiteItemsTeleport = config.Wrap("OnTeleport", "DistributeWhiteItems", "Should white items be distributed?", true);
            distributeGreenItemsTeleport = config.Wrap("OnTeleport", "DistributeGreenItems", "Should green items be distributed?", true);
            distributeRedItemsTeleport   = config.Wrap("OnTeleport", "DistributeRedItems",   "Should red items be distributed?",   true);
            distributeLunarItemsTeleport = config.Wrap("OnTeleport", "DistributeLunarItems", "Should lunar items be distributed?", false);
            distributeBossItemsTeleport  = config.Wrap("OnTeleport", "DistributeBossItems", "Should boss items be distributed?",   true);

            teleportItemBlacklist = new ItemSetConfigWrapper(config.Wrap("OnTeleport", "ItemBlacklist", "Comma-separated list of items to ignore; works with both english names and ItemIndex names.", ""));
        }

        public static CachedConfigEntry<bool> printerOverrideTarget;
        public static CachedConfigEntry<bool> distributeToDeadPlayers;
        public static CachedConfigEntry<string, AutoItemPickup.Mode> distributionModeDrop;
        public static CachedConfigEntry<string, AutoItemPickup.Mode> distributionModeTeleport;

        public static CachedConfigEntry<bool> distributeOnDrop;
        public static CachedConfigEntry<bool> distributeOnTeleport;

        public static CachedConfigEntry<bool> distributeWhiteItemsDrop;
        public static CachedConfigEntry<bool> distributeGreenItemsDrop;
        public static CachedConfigEntry<bool> distributeRedItemsDrop;
        public static CachedConfigEntry<bool> distributeLunarItemsDrop;
        public static CachedConfigEntry<bool> distributeBossItemsDrop;

        public static CachedConfigEntry<bool> distributeWhiteItemsTeleport;
        public static CachedConfigEntry<bool> distributeGreenItemsTeleport;
        public static CachedConfigEntry<bool> distributeRedItemsTeleport;
        public static CachedConfigEntry<bool> distributeLunarItemsTeleport;
        public static CachedConfigEntry<bool> distributeBossItemsTeleport;

        public static ItemSetConfigWrapper dropItemBlacklist;
        public static ItemSetConfigWrapper teleportItemBlacklist;
    }
}
