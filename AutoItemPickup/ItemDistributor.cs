﻿using System;
using System.Collections.Generic;
using System.Text;
using RoR2;

namespace AutoItemPickup
{
    public abstract class ItemDistributor
    {
        public abstract void DistributeItem(GenericPickupController item);
        public abstract void UpdateTargets();
        public ItemDistributor()
        {
            UpdateTargets();
        }
        public virtual bool IsValid()
        {
            return true;
        }

        protected static bool IsValidTarget(PlayerCharacterMasterController player)
        {
            return player.master != null && (player.master.alive || ModConfig.distributeToDeadPlayers);
        }

        protected static List<PlayerCharacterMasterController> GetValidTargets()
        {
            List<PlayerCharacterMasterController> filteredPlayers = new List<PlayerCharacterMasterController>();
            foreach(var player in PlayerCharacterMasterController.instances)
            {
                if (IsValidTarget(player))
                    filteredPlayers.Add(player);
            }
            return filteredPlayers;
        }

        public static ItemDistributor GetItemDistributor(AutoItemPickup.Mode mode)
        {
            switch(mode)
            {
                case AutoItemPickup.Mode.Closest:
                    return new ItemDistributors.ClosestDistributor();

                case AutoItemPickup.Mode.LeastItems:
                    return new ItemDistributors.LeastItemsDistributor();

                case AutoItemPickup.Mode.Random:
                    return new ItemDistributors.RandomDistributor();

                case AutoItemPickup.Mode.Sequential:
                default:
                    return new ItemDistributors.SequentialDistributor();
            }
        }
    }
}
