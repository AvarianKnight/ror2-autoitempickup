﻿using System;
using System.Collections.Generic;
using System.Text;
using RoR2;

namespace AutoItemPickup.ItemDistributors
{
    class SequentialDistributor : ItemDistributor
    {
        Inventory[] playerDistribution;
        int index = 0;

        public override void UpdateTargets()
        {
            List<PlayerCharacterMasterController> filteredPlayers = GetValidTargets();
            playerDistribution = new Inventory[filteredPlayers.Count];

            for (int i = 0; i < filteredPlayers.Count; i++)
            {
                var player = filteredPlayers[i];

                playerDistribution[i] = player.master.inventory;
            }

            index = UnityEngine.Random.Range(0, playerDistribution.Length - 1);
        }

        public override void DistributeItem(GenericPickupController item)
        {
            if (playerDistribution.Length == 0)
                return;

            AutoItemPickup.GrantItem(item, playerDistribution[index]);
            index = (++index) % playerDistribution.Length;
        }
    }
}
