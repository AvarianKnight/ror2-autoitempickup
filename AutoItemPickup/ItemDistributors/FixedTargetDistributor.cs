﻿using System;
using System.Collections.Generic;
using System.Text;
using RoR2;

namespace AutoItemPickup.ItemDistributors
{
    class FixedTargetDistributor : ItemDistributor
    {
        PlayerCharacterMasterController target;

        public FixedTargetDistributor(PlayerCharacterMasterController target)
        {
            this.target = target;
        }

        public override bool IsValid()
        {
            return IsValidTarget(target);
        }

        public override void UpdateTargets()
        {
            
        }

        public override void DistributeItem(GenericPickupController item)
        {
            AutoItemPickup.GrantItem(item, target.master.inventory);
        }
    }
}
