﻿using System;
using System.Collections.Generic;
using System.Text;
using RoR2;

namespace AutoItemPickup.ItemDistributors
{
    class RandomDistributor : ItemDistributor
    {
        Inventory[] inventories;

        public override void UpdateTargets()
        {
            List<PlayerCharacterMasterController> filteredPlayers = GetValidTargets();
            inventories = new Inventory[filteredPlayers.Count];

            for (int i = 0; i < filteredPlayers.Count; i++)
            {
                var player = filteredPlayers[i];

                inventories[i] = player.master.inventory;
            }
        }

        public override void DistributeItem(GenericPickupController item)
        {
            AutoItemPickup.GrantItem(item, inventories[UnityEngine.Random.Range(0, inventories.Length - 1)]);
        }
    }
}
