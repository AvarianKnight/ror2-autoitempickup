﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using RoR2;
using UnityEngine;

namespace AutoItemPickup.ItemDistributors
{
    class ClosestDistributor : ItemDistributor
    {
        private static FieldInfo field_deathFootPosition = typeof(CharacterMaster).GetField("deathFootPosition", BindingFlags.Instance | BindingFlags.NonPublic);
        (Inventory, Transform, Vector3?)[] inventories;

        public override void UpdateTargets()
        {
            List<PlayerCharacterMasterController> filteredPlayers = GetValidTargets();
            inventories = new (Inventory, Transform, Vector3?)[filteredPlayers.Count];
            
            for (int i = 0; i < filteredPlayers.Count; i++)
            {
                var player = filteredPlayers[i];
                var master = player.master;

                inventories[i] = (player.master.inventory, master.alive ? master.GetBodyObject()?.transform : null, master.alive ? null : (Vector3?)(Vector3)field_deathFootPosition.GetValue(master));
            }
        }

        public override void DistributeItem(GenericPickupController item)
        {
            Inventory closestPlayer = null;
            float closestDistance = float.MaxValue;
            
            Vector3 itemPosition = item.transform.position;
            
            foreach (var player in inventories)
            {
                Vector3? playerPos = player.Item2?.position ?? player.Item3;
                if (playerPos.HasValue)
                {
                    float distance = (itemPosition - playerPos.Value).sqrMagnitude;
                    if (distance < closestDistance)
                    {
                        closestPlayer = player.Item1;
                        closestDistance = distance;
                    }
                }
            }
            
            AutoItemPickup.GrantItem(item, closestPlayer);
        }
    }
}
