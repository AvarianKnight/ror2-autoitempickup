﻿using System;
using System.Collections.Generic;
using System.Text;
using RoR2;

namespace AutoItemPickup.ItemDistributors
{
    class LeastItemsDistributor : ItemDistributor
    {
        Inventory[] inventories;

        public override void UpdateTargets()
        {
            List<PlayerCharacterMasterController> filteredPlayers = GetValidTargets();
            inventories = new Inventory[filteredPlayers.Count];

            for (int i = 0; i < filteredPlayers.Count; i++)
            {
                var player = filteredPlayers[i];

                inventories[i] = player.master.inventory;
            }
        }

        public override void DistributeItem(GenericPickupController item)
        {
            Inventory leastItems = null;
            int leastItemCount = int.MaxValue;

            ItemTier tier = ItemCatalog.GetItemDef(item.pickupIndex.itemIndex).tier;

            foreach (var player in inventories)
            {
                int count = player.GetTotalItemCountOfTier(tier);
                if (count < leastItemCount)
                {
                    leastItems = player;
                    leastItemCount = count;
                }
            }
            
            AutoItemPickup.GrantItem(item, leastItems);
        }
    }
}
