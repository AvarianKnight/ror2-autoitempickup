# AutoItemPickup

Automatically grants items on the ground to players when the teleport is initiated or an item spawns.

## Features:

-   Distributing on item drop and/or teleport
-   Can grant items to dead players
-   Filtering by item tier, ignores lunar items by default
-   By default items from printers/cauldrons/similar go to the activator upon distribution, as long as the activator is still a valid target
-   Items can be blacklisted from pickup individually, using a comma-separated config entry. Items can be blacklisted by english name (EN_US locale) or ItemIndex name. For example:
    -   `ItemBlacklist = Bustling Fungus, Syringe, Infusion, BleedOnHit, Backup Magazine`
-   Features 4 distribution modes:
    -   `Sequential` - Grants one item to each player, looping over players when they run out, roughly equally distributed
    -   `Random` - Distributes items randomly
    -   `Closest` - Grants each item to the nearest player
    -   `LeastItems` - Grants each item to the player with least items of its rarity

## Installation

Copy the `AutoItemPickup` folder to `Risk of Rain 2/BepInEx/plugins`

## Patch notes:

- 1.2.1 - Fixed error related to changes in language handling in latest RoR2 update (Build #3830295)
- 1.2.0 - Added item blacklisting, separate for drop and teleport.
- 1.1.4 - Fixed missing UpdateTargets condition, which caused `Closest` distributor to throw after first stage. Added additional exception handling in distributor to avoid interfering with game code.
- 1.1.3 - Fixed `Closest` distribution mode throwing exceptions and preventing teleportation when dead players are not ignored and a player is dead
- 1.1.2 - Fixed `Closest` distribution mode presumably being completely broken, without actually testing to make sure the fix isn't broken. Have fun!
- 1.1.1 - Added `OverridePrinterTarget` config option that ensures the activator receives printer result as long as they're valid
- 1.1.0
    - Added on-drop distribution setting
    - Made on-teleport distribution togglable
    - Reworked code structure
    - Fixed wrong code that could've lead to errors when ignoring dead players
    - Added filtering for white, green, red and boss items
    - Added separate filtering options for teleport and drop
- 1.0.2 - Added config option for ignoring lunar items
- 1.0.1 - Added website url
- 1.0.0 - Initial release